package sk.tenlines.java.pohovor;


public interface Task4 {
    
    /**
     * Checks if given string contains valid set of parenthesis.
     * Permited parenthesis are: ()[]{}
     * Valid input is e.g.: "([]{}{({[]})})"
     * Invalid input e.g.: "[([)]]","{(})", "((((("
     */ 
    public boolean isValidParenthesis(String inputString);
    
    
    
    /**
     * Checks if given string contains valid set of parenthesis.
     * Permited parenthesis are: ()[]{}
     * Valid input is e.g.: "([]{}{({[]})})"
     * Invalid input e.g.: "[([)]]","{(})", "((((("
     */ 
    public boolean isValidParenthesisLevelTwo(String inputString);
    
}