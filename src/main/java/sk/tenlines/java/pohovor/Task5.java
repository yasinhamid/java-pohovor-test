package sk.tenlines.java.pohovor;


/**
 * Implement as a singleton.
 * On creation generates random number from <0, 100>.
 * On every call getNumber() returns generated number.
 * 
 */ 
public interface Task5 {
   
   
   public Task5 getInstance();
   
   public int getNumber();
    
    
}