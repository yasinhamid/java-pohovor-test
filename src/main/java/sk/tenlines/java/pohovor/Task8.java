package sk.tenlines.java.pohovor;

import java.io.OutputStream;

/**
 * Write Queue, sender and receiver implementation.
 * Both sender and receiver are started by calling start
 * 
 * 
 */ 
public interface Task8 {

    public Queue getNewQueue();
    public Sender getNewSender();
    public Receiver getNewReceiver();

    
    public interface Queue {
        public void push(String input);
        public String pull();
    }
    
    public interface Sender {
        public void start(Queue q);
        public void send(String toSend);
        public void terminate();
    }

    /**
     * Receiver on receiving message writes it to output stream.
     * The receiver is stopped after receiving message starting with X.
     *
     */
    public interface Receiver {
        public void start(Queue q, OutputStream stream);
    }
    
}