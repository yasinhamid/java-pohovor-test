package sk.tenlines.java.pohovor;

public interface Task6 {
    
    public String convertToDecimal(String hexNUmber);
    
    public String convertToHex(String decimalNumber);
    
}