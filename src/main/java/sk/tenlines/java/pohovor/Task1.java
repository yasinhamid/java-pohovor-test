package sk.tenlines.java.pohovor;

import java.util.List;

/**
 * Write a program 
 * 
 */
public interface Task1 {
    
    
    /**
     * Reverse input string.
     * e.g. input = "This is an input" returns "tupni na si sihT"
     * 
     */ 
    public String reverseString(String input);
    
    
    /**
     * Returns fibonacci series. Every number is a sum of previous two
     * 0, 1, 1, 2, 3, 5, 8, 13
     * 0, 1, 1=0+1, 2=1+1, 3=1+2, 5=2+3, 8=3+5, 13=5+8
     * 
     */ 
    public List<Long> returnFibonacciSeries(long maxLimit);
    
    
    /**
     * If the number is divisible by 3 returns instead 'Fizz'
     * If the number is divisible by 5 returns instead 'Buzz'
     * If the number is divisible by 3 and 5 returns 'FizzBuzz'
     * 
     */ 
    public String fizzBuzzProblem(long number);
    
    
    /**
     * A number is armstrong if sum of
     * digidits powered to the number of digits
     * is equal to the number.
     * 
     * e.g. 153 is Armstrong number
     * 153 = 1^3 + 5^3 + 3^3
     * 
     * 
     * 
     */ 
    public boolean isArmstrongNumber(long number);
    
    
    
    
    
}