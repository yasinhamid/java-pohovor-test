package sk.tenlines.java.pohovor;

import java.math.BigDecimal;
import java.util.List;
/**
 * 
 * 
 * 
 */ 
public class Task3 {
   
   public interface Employee {
       
       public void setName(String name);
       public void setSurname(String surname);
       public void setSalary(BigDecimal salary);
       
       public BigDecimal getSalary();
       
       /**
        * Returns "<FIRST NAME> <SURNAME>";
        */ 
       public BigDecimal getFullName();
   }

   public Employee getNewEmployee() {
       //TODO Implement returning new object of employee.
       return null;
   }

   
   /**
    * Returns input array of Employee list sorted by salary in ascending order
    */ 
   public List<Task3.Employee> sortBySalaryAsc(List<Task3.Employee> employees) {
       //TODO implement
       return null;
   }
   
   /**
    * Returns input array of Employee list sorted by salary in descending order
    */
   public List<Employee> sortBySalaryDesc(List<Employee> employees) {
       //TODO implement
       return null;
   }
    
}