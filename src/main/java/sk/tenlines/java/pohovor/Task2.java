package sk.tenlines.java.pohovor;


import java.util.Date;

/**
 * Create class that impelments Employee interface with following fields:
 * name - first name of the person
 * surname - last name of the person
 * birthDate - birth date
 * personalId - id used by company to identify the employee
 * 
 * Implement code for comparing two objects of Employee class.
 * 
 */ 
public class Task2 {
    
    public interface Employee {
        public void setName(String name);
        public void setSurname(String surname);
        public void setBirthdate(Date birthDate);
        public void setPersonalId(Long personalId);
    }
    
    public Employee getNewEmployee() {
        //TODO implement creation of an empty employee object
        return null;
    }
    
    
    
}