package sk.tenlines.java.pohovor;
import java.util.List;

public interface Task7 {
    
    /**
     * Returns the number of words in a given file.
     * 
     */ 
    public Long wordCount(String inputFileName);
    
    
    /**
     * Returns the longest line from a given file.
     * 
     */ 
    public String longestLine(String inputFileName);
    
    
    /**
     * Returns matrix 8x8 from data in a given file.
     * With B or A on positions given by the data
     * in given file. Empty spaces are filled by 0
     * Number of lines in file is arbitraty.
     * Format:
     * <X> <Y> <TYPE>
     * 
     * x and y can be from<0,8>
     * TYPE: A or B
     * 
     * e.g.
     * Input file:
     * 3 3 A
     * 0 0 B
     * 1 1 B
     * 2 2 B
     * 
     * returns:
     * 00000000
     * 00000000
     * 00000000
     * 00000000
     * 000A0000
     * 00B00000
     * 0B000000
     * B0000000
     * 
     */ 
    public List<String> constructBattleshipMatrix(String inputFileName);
    
}