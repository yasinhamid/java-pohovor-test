package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;

import java.util.List;

import static junit.framework.Assert.*;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

/**
 * Write a program 
 * 
 */
public class Task1_Test {
    
    Task1 taskService = new Task1Impl();
    
    @Test 
    public void reverseStringWithoutSpaces() {
        
        assertEquals("secapStuohtiWgnirtSsIsihT", taskService.reverseString("ThisIsStringWithoutSpaces"));
    }
    
    @Test
    public void reverseStringWithSpaces() {
        assertEquals("secapS tuohtiW gnirtS sI sihT", taskService.reverseString("This Is String Without Spaces"));
    }
    
    @Test 
    public void reverseStringWithSpacesAllUpercase() {
        assertEquals("TXET REPPU EMOS", taskService.reverseString("SOME UPPER TEXT"));
    }
    
    @Test 
    public void reverseStringWithSpacesAllLowercase() {
        assertEquals("esac rewol emos", taskService.reverseString("some lower case"));
    }
    
    @Test 
    public void reverseStringWithLeadingSpaces() {
        assertEquals(" txet enO si ereht ", taskService.reverseString(" there is One text "));
    }
    
    
    /**
     * Returns fibonacci series. Every number is a sum of previous two
     * 0, 1, 1, 2, 3, 5, 8, 13
     * 0, 1, 1=0+1, 2=1+1, 3=1+2, 5=2+3, 8=3+5, 13=5+8
     * 
     */
    @Test
    public void returnFibonacciSeriesUpToTen(){
        List<Long> result = taskService.returnFibonacciSeries(10);
        assertArrayEquals(new Long[]{0l,1l,1l,2l,3l,5l,8l,13l,21l,34l}, result.toArray(new Long[result.size()]));
    }
    
    @Test
    public void returnFibonacciSeriesUpTo5(){
        List<Long> result = taskService.returnFibonacciSeries(5);
        assertArrayEquals(new Long[]{0l,1l,1l,2l,3l}, result.toArray(new Long[result.size()]));
    }
    
    @Test
    public void returnFibonacciSeriesUpTo15(){
        List<Long> result = taskService.returnFibonacciSeries(15);
        assertArrayEquals(new Long[]{0l,1l,1l,2l,3l,5l,8l,13l,21l,34l,55l,89l,144l,233l,377l}, result.toArray(new Long[result.size()]));
    }
    
    @Test
    public void returnFibonacciSeriesUpTo0(){
        List<Long> result = taskService.returnFibonacciSeries(0);
        assertArrayEquals(new Long[]{}, result.toArray(new Long[result.size()]));
    }
    
    @Test
    public void returnFibonacciSeriesUpTo1(){
        List<Long> result = taskService.returnFibonacciSeries(1);
        assertArrayEquals(new Long[]{0l}, result.toArray(new Long[result.size()]));
    }
    
    @Test
    public void returnFibonacciSeriesUpTo2(){
        List<Long> result = taskService.returnFibonacciSeries(2);
        assertArrayEquals(new Long[]{0l, 1l}, result.toArray(new Long[result.size()]));
    }
    
    
    /**
     * If the number is divisible by 3 returns instead 'Fizz'
     * If the number is divisible by 5 returns instead 'Buzz'
     * If the number is divisible by 3 and 5 returns 'FizzBuzz'
     * 
     */ 
    @Test 
    public void fizzBuzzProblem_Test_All_Variants() {
        assertEquals("Fizz",taskService.fizzBuzzProblem(3));
        assertEquals("Buzz",taskService.fizzBuzzProblem(5));
        assertEquals("FizzBuzz",taskService.fizzBuzzProblem(15));
        assertEquals("",taskService.fizzBuzzProblem(7));
        assertEquals("Fizz",taskService.fizzBuzzProblem(129));
        assertEquals("Buzz",taskService.fizzBuzzProblem(125));
        assertEquals("FizzBuzz",taskService.fizzBuzzProblem(30));
        assertEquals("",taskService.fizzBuzzProblem(127));
    }
    
    
    /**
     * A number is armstrong if sum of
     * digidits powered to the number of digits
     * is equal to the number.
     * 
     * e.g. 153 is Armstrong number
     * 153 = 1^3 + 5^3 + 3^3
     * 
     * 
     * 
     */
     @Test
    public void test_153_isArmstrongNumber_True() {
        assertTrue(taskService.isArmstrongNumber(153));
    }
    
    @Test
    public void test_154_isArmstrongNumber_False() {
        assertFalse(taskService.isArmstrongNumber(154));
    }
    
    @Test
    public void test_Armstrong_Numbers_Up_To_1000_True() {
        assertTrue(taskService.isArmstrongNumber(0));
        assertTrue(taskService.isArmstrongNumber(1));
        assertTrue(taskService.isArmstrongNumber(153));
        assertTrue(taskService.isArmstrongNumber(370));
        assertTrue(taskService.isArmstrongNumber(371));
        assertTrue(taskService.isArmstrongNumber(407));
    }
    
    @Test
    public void test_Numbers_Below_1000_isArmstrongNumber_False() {
        assertFalse(taskService.isArmstrongNumber(154));
        assertFalse(taskService.isArmstrongNumber(111));
        assertFalse(taskService.isArmstrongNumber(33));
        assertFalse(taskService.isArmstrongNumber(21));
        assertFalse(taskService.isArmstrongNumber(721));
    }
    
    
    
    
    
}