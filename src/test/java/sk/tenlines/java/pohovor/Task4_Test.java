package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;
import static junit.framework.Assert.*;

public class Task4_Test {

    Task4 taskService; //= new Task4Impl();


    @Test
    public void test_isValidParenthesis_True() {
        assertTrue(taskService.isValidParenthesis(""));
        assertTrue(taskService.isValidParenthesis("([]{}{({[]})})"));
        assertTrue(taskService.isValidParenthesis("({}{({[]})})"));
        assertTrue(taskService.isValidParenthesis("([]{}{({})})"));
        assertTrue(taskService.isValidParenthesis("([]{}{{[]}})"));
        assertTrue(taskService.isValidParenthesis("([]{({[]})})"));
        assertTrue(taskService.isValidParenthesis("([]{there}{({[is ]}some )}text)"));
    }

    @Test
    public void test_isValidParenthesis_False() {
        assertFalse(taskService.isValidParenthesis("[([)]]"));
        assertFalse(taskService.isValidParenthesis("{(})"));
        assertFalse(taskService.isValidParenthesis("((((("));
        assertFalse(taskService.isValidParenthesis("((())("));
        assertFalse(taskService.isValidParenthesis("(({)}("));
    }

}