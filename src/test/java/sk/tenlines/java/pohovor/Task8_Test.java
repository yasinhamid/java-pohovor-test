package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;

import java.io.BufferedOutputStream;
import java.io.OutputStream;

import static junit.framework.Assert.*;

/**
 * Write Queue, sender and receiver implementation.
 * Both sender and receiver are started by calling start
 * 
 * 
 */ 
public class Task8_Test {
/*
    Task8 taskService; //= new Task8Impl();

    @Test
    public void foo() {
        Task8.Queue queue = taskService.getNewQueue();
        Task8.Sender sender = taskService.getNewSender();
        Task8.Receiver receiver = taskService.getNewReceiver();

        OutputStream stream = new BufferedOutputStream();

        sender.start(queue);
        receiver.start(queue);

        sender.send("First message");
        sender.send("Second message");

        assertEquals("First message", receiver.getMessage());



    }

    
    public interface Queue {
        public void push(String input);
        public String pull();
    }
    
    public interface Sender {
        public void start(Queue q);
        public void send(String toSend);
        public void terminate();
    }
    
    public interface Receiver {
        public void start(Queue q);
    }
  */  
}