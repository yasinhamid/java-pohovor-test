package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;
import static junit.framework.Assert.*;

public class Task5_Test {


   /**
    * This is far from comprehensive test.
    * Because it doesn't test if two threads doesn't accidentaly create
    * two instances of the Task5 class;
    *
    */
   @Test
   public void test_Task5_returns_singleton() {
      Task5 taskService1 = null; //= new Task5Impl();
      Task5 task5 = taskService1.getInstance();
      Task5 taskService2 = null; //= new Task5Impl();
      Task5 task5_2 = taskService2.getInstance();

      assertTrue(task5 == task5_2);
   }

    
}