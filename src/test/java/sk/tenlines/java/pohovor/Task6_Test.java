package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;
import static junit.framework.Assert.*;

public class Task6_Test {

    Task6 taskService;// = new Task6Impl();

    @Test
    public void convert_3_to_hex_0x03() {
        assertEquals("3", taskService.convertToHex("3"));
    }



    @Test
    public void convert_8_to_hex_0x08() {
        assertEquals("8", taskService.convertToHex("8"));
    }
    @Test
    public void convert_15_to_hex_0x0F() {
        assertEquals("F", taskService.convertToHex("15"));
    }

    @Test
    public void convert_16_to_hex_0x10() {
        assertEquals("10", taskService.convertToHex("16"));
    }

    @Test
    public void convert_31_to_hex_0x1F() {
        assertEquals("1F", taskService.convertToHex("31"));
    }

    @Test
    public void convert_15790320_to_hex_0xF0F0F0() {
        assertEquals("F0F0F0", taskService.convertToHex("15790320"));
    }

    @Test
    public void convert_0x8_to_decimal_8() {
        assertEquals("8", taskService.convertToDecimal("8"));
    }

    @Test
    public void convert_0xF_to_decimal_15() {
        assertEquals("15", taskService.convertToDecimal("F"));
    }

    @Test
    public void convert_0x10_to_decimal_16() {
        assertEquals("16", taskService.convertToDecimal("10"));
    }

    @Test
    public void convert_0x1F_to_decimal_31() {
        assertEquals("31", taskService.convertToDecimal("1F"));
    }

    @Test
    public void convert_0x1F23456_to_decimal_32650326() {
        assertEquals("32650326", taskService.convertToDecimal("1F23456"));
    }


}
