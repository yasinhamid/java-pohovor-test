package sk.tenlines.java.pohovor;

import java.util.Date;

import org.testng.annotations.Test;
import static junit.framework.Assert.*;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class Task2_Test {
    
    
    Task2 taskService = new Task2();
    
    @Test
    public void compare_object_to_null_false() {
        
        Task2.Employee employee = taskService.getNewEmployee();
        assertFalse(employee.equals(null));
        
    }
    
    @Test
    public void compare_object_to_itself_true() {
        Task2.Employee employee = taskService.getNewEmployee();
        assertTrue(employee.equals(employee));
    }
    
    @Test
    public void compare_two_object_with_the_same_attributes_true() {
        Task2.Employee employee1 = taskService.getNewEmployee();
        employee1.setName("Johnny");
        employee1.setSurname("Bravo");
        employee1.setBirthdate(new Date(1990,7,23));
        employee1.setPersonalId(11150l);
        
        Task2.Employee employee2 = taskService.getNewEmployee();
        employee2.setName("Johnny");
        employee2.setSurname("Bravo");
        employee2.setBirthdate(new Date(1990,7,23));
        employee2.setPersonalId(11150l);
        assertTrue(employee1.equals(employee2));
    }
    
    @Test
    public void compare_two_object_different_name_false() {
        Task2.Employee employee1 = taskService.getNewEmployee();
        employee1.setName("Johnny");
        employee1.setSurname("Bravo");
        employee1.setBirthdate(new Date(1990,7,23));
        employee1.setPersonalId(11150l);
        
        Task2.Employee employee2 = taskService.getNewEmployee();
        employee2.setName("Antony");
        employee2.setSurname("Bravo");
        employee2.setBirthdate(new Date(1990,7,23));
        employee2.setPersonalId(11150l);
        assertFalse(employee1.equals(employee2));
    }
    
    @Test
    public void compare_two_object_different_surnamename_false() {
        Task2.Employee employee1 = taskService.getNewEmployee();
        employee1.setName("Johnny");
        employee1.setSurname("Bravo");
        employee1.setBirthdate(new Date(1990,7,23));
        employee1.setPersonalId(11150l);
        
        Task2.Employee employee2 = taskService.getNewEmployee();
        employee2.setName("Johnny");
        employee2.setSurname("Bravado");
        employee2.setBirthdate(new Date(1990,7,23));
        employee2.setPersonalId(11150l);
        assertTrue(employee1.equals(employee2));
    }
    
    @Test
    public void compare_two_object_different_birthdate_false() {
        Task2.Employee employee1 = taskService.getNewEmployee();
        employee1.setName("Johnny");
        employee1.setSurname("Bravo");
        employee1.setBirthdate(new Date(1990,7,23));
        employee1.setPersonalId(11150l);
        
        Task2.Employee employee2 = taskService.getNewEmployee();
        employee2.setName("Johnny");
        employee2.setSurname("Bravo");
        employee2.setBirthdate(new Date(1990,7,1));
        employee2.setPersonalId(11150l);
        assertTrue(employee1.equals(employee2));
    }
    
    @Test
    public void compare_two_object_different_personalId_false() {
        Task2.Employee employee1 = taskService.getNewEmployee();
        employee1.setName("Johnny");
        employee1.setSurname("Bravo");
        employee1.setBirthdate(new Date(1990,7,23));
        employee1.setPersonalId(11150l);
        
        Task2.Employee employee2 = taskService.getNewEmployee();
        employee2.setName("Johnny");
        employee2.setSurname("Bravo");
        employee2.setBirthdate(new Date(1990,7,23));
        employee2.setPersonalId(21150l);
        assertTrue(employee1.equals(employee2));
    }
    
    
}