package sk.tenlines.java.pohovor;
import org.testng.annotations.Test;
import static junit.framework.Assert.*;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class Task7_Test {

    private static final String wordCount_txt = "resources/wordCount.txt";
    private static final String matrix1_txt = "resources/matrix1.txt";
    private static final String matrix2_txt = "resources/matrix2.txt";

    Task7 taskService; //= new Task7Impl();

    @Test
    public void test_wordCount_from_wordCount_txt_243() {
        assertEquals(new Long(243),taskService.wordCount(wordCount_txt));
    }


    @Test
    public void test_longest_line_from_file() {
        assertEquals(
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
                taskService.longestLine(wordCount_txt));
    }

    @Test
    public void test_battleship_matrix_from_matrix1_txt() {
        String[] expectedResult = new String[] {
                "00000000",
                "00000000",
                "00000000",
                "00000000",
                "000A0000",
                "00B00000",
                "0B000000",
                "B0000000"
        };

        assertArrayEquals(expectedResult, taskService.constructBattleshipMatrix(matrix1_txt).toArray());
    }

    @Test
    public void test_battleship_matrix_from_matrix2_txt() {
        String[] expectedResult = new String[] {
                "000000A0",
                "00A00000",
                "00000000",
                "0000BB00",
                "00000000",
                "00BBB000",
                "00000000",
                "00000AA0"
        };

        assertArrayEquals(expectedResult, taskService.constructBattleshipMatrix(matrix2_txt).toArray());
    }
}