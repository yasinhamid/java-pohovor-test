package sk.tenlines.java.pohovor;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

public class Task3_Test {


   Task3 taskService = new Task3();

   @Test
   public void test_getFullName() {
       Task3.Employee employee = taskService.getNewEmployee();
       employee.setName("Johnny");
       employee.setSurname("Bravo");

       assertEquals("Johnny Bravo", employee.getFullName());
   }

   @Test
   public void test_Order_Employees_Ascending() {
       List<Task3.Employee> employeeList = new ArrayList<>();
       createTestEmploeeList(employeeList);

       List<Task3.Employee> sortedList = taskService.sortBySalaryAsc(employeeList);
       assertEquals("Bravo 1", sortedList.get(0));
       assertEquals("Bravo 1000", sortedList.get(1));
       assertEquals("Bravo 1000.1", sortedList.get(2));
       assertEquals("Bravo 1500", sortedList.get(3));
       assertEquals("Bravo 2000", sortedList.get(4));
   }

    @Test
    public void test_Order_Employees_Descending() {
        List<Task3.Employee> employeeList = new ArrayList<>();
        createTestEmploeeList(employeeList);

        List<Task3.Employee> sortedList = taskService.sortBySalaryAsc(employeeList);

        assertEquals("Bravo 2000", sortedList.get(0));
        assertEquals("Bravo 1500", sortedList.get(1));
        assertEquals("Bravo 1000.1", sortedList.get(2));
        assertEquals("Bravo 1000", sortedList.get(3));
        assertEquals("Bravo 1", sortedList.get(4));
    }

    private void createTestEmploeeList(List<Task3.Employee> employeeList) {
        {
            Task3.Employee employee = taskService.getNewEmployee();
            employee.setName("Johnny");
            employee.setSurname("Bravo 1");
            employee.setSalary(new BigDecimal("1"));
            employeeList.add(employee);
        }
        {
            Task3.Employee employee = taskService.getNewEmployee();
            employee.setName("Johnny");
            employee.setSurname("Bravo 1000.1");
            employee.setSalary(new BigDecimal("1000.1"));
            employeeList.add(employee);
        }
        {
            Task3.Employee employee = taskService.getNewEmployee();
            employee.setName("Johnny");
            employee.setSurname("Bravo 1000");
            employee.setSalary(new BigDecimal("1000"));
            employeeList.add(employee);
        }
        {
            Task3.Employee employee = taskService.getNewEmployee();
            employee.setName("Johnny");
            employee.setSurname("Bravo 2000");
            employee.setSalary(new BigDecimal("2000"));
            employeeList.add(employee);
        }
        {
            Task3.Employee employee = taskService.getNewEmployee();
            employee.setName("Johnny");
            employee.setSurname("Bravo 1500");
            employee.setSalary(new BigDecimal("1500"));
            employeeList.add(employee);
        }
    }


}